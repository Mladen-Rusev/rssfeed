package RssReader

import (
	"encoding/xml"
	"log"
	"os"
	"strconv"
	"testing"
)

func TestToRssItem(t *testing.T) {
	xmlContent, err := os.ReadFile("test.xml")
	if err != nil {
		log.Fatalln("Could not read test xml file!")
	}
	data := &rss{}
	err = xml.Unmarshal(xmlContent, data)

	rssItem := feedToRssItem(data)
	if len(rssItem) != 26 {
		t.Errorf("Got less RssItems than expected!!")
	}
}

func TestParse(t *testing.T) {
	urls := []string{"https://rss.nytimes.com/services/xml/rss/nyt/Technology.xml", "https://medium.com/feed/tag/programming", "https://www.themarginalian.org/feed/"}
	items := Parse(urls)
	log.Println("Created " + strconv.Itoa(len(items)) + " rssItems!")
	if len(items) == 0 {
		t.Errorf("Empty results! RssItems could not be parsed..")
	}
}
