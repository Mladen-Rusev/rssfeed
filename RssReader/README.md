# Foobar

RssReader is a Golang library for concurrently parsing RSS feed URLs and extracting their items

## Installation

You can import the package via

```go
import "gitlab.com/Mladen-Rusev/rssfeed/RssReader"

```

## Usage
Call Parse method, passing a collection of RSS feed urls.
The xml rss feed will be parsed and its contents returned.
```go
import foobar
urls := []string{"https://rss.nytimes.com/services/xml/rss/nyt/Technology.xml"
,"https://medium.com/feed/tag/programming"
,"https://www.themarginalian.org/feed/"}
items := Parse(urls)

```
