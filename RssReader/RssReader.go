package RssReader

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
	"strings"
	"sync"
	"time"
)

//Parse concurrently makes requests to passed RSS feed urls and extracts their items.
//Once all URLs have been processed, returns a collection of all parsed RssItems
func Parse(urls []string) []RssItem {
	var wg sync.WaitGroup
	wg.Add(len(urls))
	sem := make(semaphore, runtime.NumCPU())
	res := make([]RssItem, 0)
	lock := sync.RWMutex{}
	defer recoverFunc()
	for _, url := range urls {
		sem.acquire(1)
		go func(url string) {
			defer sem.release(1)
			resp, err := http.Get(strings.ReplaceAll(url, " ", ""))
			if err != nil {
				panic(err)
			}
			defer resp.Body.Close()
			body, _ := ioutil.ReadAll(resp.Body)
			data := &rss{}
			d := xml.NewDecoder(bytes.NewReader(body))
			d.DefaultSpace = "RssDefault" //handle cases when link tag is in format atom:link
			err = d.Decode(data)
			if err != nil {
				log.Println("Error encountered during XML unmarshal")
			}
			rssItems := feedToRssItem(data)
			lock.Lock()
			res = append(res, rssItems...)
			lock.Unlock()
			wg.Done()
		}(url)
	}
	wg.Wait()
	return res
}

func recoverFunc() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("Recovering from panic in Parse; error is: %v \n", r)
		}
	}()
}

func feedToRssItem(feed *rss) []RssItem {
	convertedItems := make([]RssItem, len(feed.ItemList))
	for i, item := range feed.ItemList {
		parsedTime, err := ParseTime(item.PubDate)
		if err != nil {
			log.Println(err)
			log.Println("Time string: " + item.PubDate)
		}
		convertedItems[i] = RssItem{
			Title:       item.Title,
			Source:      feed.Title,
			SourceURL:   feed.Link.Href,
			Link:        item.LinkString,
			PublishDate: parsedTime,
			Description: item.Description,
		}
	}
	return convertedItems
}

//Encountered different time formats so included them in a collection
var timeFormats = []string{
	"Mon, 02 Jan 2006 15:04:05 -0700",
	"Mon 02 Jan 2006 15:04:05 -0700",
	"Mon, 02 Jan 2006 15:04:05 MST",
}

func ParseTime(text string) (t time.Time, err error) {
	for _, layout := range timeFormats {
		t, err = time.Parse(layout, text)
		if err == nil {
			return
		}
	}
	return t, fmt.Errorf("error parsing time")
}
