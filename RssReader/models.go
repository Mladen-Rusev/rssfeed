package RssReader

import (
	"encoding/xml"
	"time"
)

type rss struct {
	XMLName     xml.Name `xml:"rss"`
	Version     string   `xml:"version,attr"`
	Title       string   `xml:"channel>title"`
	Link        link     `xml:"channel>link"`
	Description string   `xml:"channel>description"`
	PubDate     string   `xml:"channel>pubDate"`
	ItemList    []item   `xml:"channel>item"`
}

type item struct {
	Title       string `xml:"title"`
	LinkString  string `xml:"RssDefault link"`
	Source      string
	SourceURL   string
	Description string `xml:"description"`
	PubDate     string `xml:"pubDate"`
}

type link struct {
	Href string `xml:"href,attr"`
}

type RssItem struct {
	Title       string
	Source      string
	SourceURL   string
	Link        string
	PublishDate time.Time
	Description string
}
