package main

import (
	"gitlab.com/Mladen-Rusev/rssfeed/RssReader"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	wordPtr := flag.String("urls", "", "A single comma separated string of Rss feed urls")
	flag.Parse()
	urls := strings.Split(*wordPtr, ",")
	log.Println("Found " + strconv.Itoa(len(urls)) + " urls from args ")
	items := RssReader.Parse(urls)
	f, err := os.Create("rss.json")
	if err != nil {
		log.Fatalln("error creating file")
	}
	defer f.Close()
	jsonBytes, err := json.MarshalIndent(items, "", " ")
	if err != nil {
		log.Println("Error marshalling RssItem!")
	}
	if _, err := f.Write(jsonBytes); err != nil {
		log.Println(err)
	}
	err = f.Sync()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(string(jsonBytes))
}
