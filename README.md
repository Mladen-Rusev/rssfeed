# RssFeedApp

This app depends on the RssReader package, which receives an array of Rss feed urls and returns a collection of their items
Parsed contents are stored in a file in json form, as well as printed on the console.

## Installation

To install go to a folder where you want to set up the project and clone the repo

```bash
git clone https://gitlab.com/Mladen-Rusev/rssfeed.git
```
Open to cloned directory and in a terminal run
```go
go get
go build
```
This will generate the binary for the app

## Usage
Run the created binary and pass a flag named urls.
To pass multiple urls use a single comma separated string of urls
```bash
go run .\main.go -urls=https://rss.nytimes.com/services/xml/rss/nyt/Technology.xml

go run .\main.go -urls=https://rss.nytimes.com/services/xml/rss/nyt/Technology.xml,https://medium.com/feed/tag/programming,https://www.themarginalian.org/feed/

```
